# how to build and run anilinker

- install prequisities:
  - [Node.js](https://nodejs.org/en/),
  - [Yarn](https://yarnpkg.com/en/),
  - [PostgreSQL](https://www.postgresql.org/),
  - [Redis](https://redis.io/),
- configurate:
  - copy `.env.example` file to `.env` and fill with your data,
  - create `ormconfig.real.js` file and fill with your postgres connection data,
- install dependencies:
  ```sh
  $ yarn
  ```
- build:
  ```sh
  $ yarn build
  ```
- run:
  ```sh
  $ yarn start
  ```