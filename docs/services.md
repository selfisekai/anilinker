# list of anilinker service names

**this list could and will grow in time**

- `amazon` - [Amazon Prime Video](https://www.primevideo.com)
- `anidb` - [AniDB.net](https://anidb.net) (anime-only service)
- `anilist` - [AniList.co](https://anilist.co) (anime and manga IDs are unified)
- `animelab` - [AnimeLab](https://animelab.com)
- `ann` - [AnimeNewsNetwork.com encyclopedia](https://www.animenewsnetwork.com/encyclopedia) (anime and manga IDs are unified)
- `crunchyroll` - [Crunchyroll](https://www.crunchyroll.com)
- `funimation` - [Funimation](https://funimation.com)
- `hidive` - [Hidive.com](https://hidive.com)
- `hulu` - [Hulu](https://hulu.com)
- `kitsu-anime` - [Kitsu.io](https://kitsu.io)/anime
- `kitsu-manga` - [Kitsu.io](https://kitsu.io)/manga
- `mal-anime` - [MyAnimeList.net](https://myanimelist.net)/anime
- `mal-manga` - [MyAnimeList.net](https://myanimelist.net)/manga
- `netflix` - [Netflix](https://netflix.com)
- `viewster` - [Viewster](https://www.viewster.com)
- `viz` - [Viz.com](https://viz.com) (anime and manga IDs are unified)
- `vrv` - [VRV](https://vrv.co)
