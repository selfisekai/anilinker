// do NOT insert your data here
// create your overrides in ormconfig.real.js file

const realConfig = require('./ormconfig.real');

// is this running with ts-node or node
const IS_TS_NODE = process.argv.some(e => /(ts-node|\.tsx?)$/.test(e));

module.exports = Object.assign({
  host: '127.0.0.1',
  username: 'postgres',
  password: 'postgres',
  database: 'postgres',
  type: 'postgres',
  synchronize: true,
  entities: [
    IS_TS_NODE ? 'src/entities/*.ts': 'dist/entities/*.js',
  ],
}, realConfig || {});
