import { gql, ApolloServer, IResolvers } from 'apollo-server-koa';
import Koa from 'koa';
import { getConnection } from 'typeorm';
import ow from 'ow';
import log from 'consola';
import { Media } from './entities/media';

export default async (koa: Koa) => {
  const typeDefs = gql`
    type Media {
      id: Int!
      type: MediaType!
      subtype: MediaSubtype
      nsfw: Boolean
      names: [MediaName!]!
      externalIds: [MediaExternalId!]!
      thumbnails: [MediaThumbnail!]!
    }

    type MediaExternalId {
      id: Int!
      service: String!
      externalId: String
      url: String!
    }

    type MediaName {
      id: Int!
      name: String!
      lang: String
      main: Boolean!
      romaji: Boolean!
      official: Boolean!
      alternative: Boolean!
    }

    type MediaThumbnail {
      id: Int!
      url: String!
    }

    enum MediaType {
      ANIME
      MANGA
    }

    enum MediaSubtype {
      # anime
      TV
      TV_SHORT
      OVA
      ONA
      MOVIE
      SPECIAL
      MUSIC

      # manga
      MANGA
      NOVEL
      ONE_SHOT
      DOUJIN
    }

    type PageInfo {
      page: Int!
      perPage: Int!
    }

    type MediaPage {
      page: [Media!]!
      pageInfo: PageInfo!
    }

    type Query {
      Media(
        id: Int
      ): Media

      MediaList(
        page: Int! = 0
        perPage: Int! = 50
        type: MediaType
        nsfw: Boolean
        search: String
      ): MediaPage!
    }
  `;

  const connection = getConnection();
  const mediaRepository = connection.getRepository(Media);

  const resolvers: IResolvers<any, any> = {
    Query: {
      Media: (parent: any, filter: any) => {
        return mediaRepository.findOne({
          where: {
            id: filter.id,
          },
          relations: ['names', 'externalIds', 'thumbnails'],
        });
      },
      MediaList: async (parent: any, filter: any) => {
        ow(filter.page, ow.number.integer);
        ow(filter.perPage, ow.number.integer.lessThanOrEqual(50));
        ow(filter.nsfw, ow.optional.boolean);
        ow(filter.search, ow.optional.string.nonEmpty);
        const query = await mediaRepository
          .createQueryBuilder('media')
          .select()
          .take(filter.perPage)
          .skip(filter.page * filter.perPage)
          .leftJoinAndSelect('media.names', 'names')
          .leftJoinAndSelect('media.externalIds', 'externalIds')
          .leftJoinAndSelect('media.thumbnails', 'thumbnails');
        if (filter.type) {
          query.andWhere('media.type = :type', { type: filter.type });
        }
        if (filter.nsfw) {
          query.andWhere('media.nsfw = :nsfw', { nsfw: filter.nsfw });
        }
        if (filter.search) {
          // a CPU hog, don't hesitate to migrate to Apache Solr
          // https://gitlab.com/selfisekai/issues/12
          query.andWhere('to_tsvector(names.name) @@ to_tsquery(:namets)', { namets: (filter.search as string).split(' ').join(' & ') });
        }
        return {
          pageInfo: {
            page: filter.page,
            perPage: filter.perPage,
          },
          page: await query.getMany(),
        };
      },
    },
  };

  const apollo = new ApolloServer({ typeDefs, resolvers });

  apollo.applyMiddleware({ app: koa });
};
