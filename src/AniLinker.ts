import 'reflect-metadata';

import dotenv from 'dotenv';
import dotenvExpand from 'dotenv-expand';
dotenvExpand(dotenv.config());

import { createConnection, Connection } from 'typeorm';
import fs from 'fs-extra';
import path from 'path';
import log from 'consola';
import { Media } from './entities/media';
import { MediaName } from './entities/media-name';
import { MediaExternalId } from './entities/media-external-id';
import { BaseService, IBaseService, ServiceState } from './BaseService';
import { MediaThumbnail } from './entities/media-thumbnail';

export default class AniLinker {
  connection: Connection;
  services: (BaseService & IBaseService)[] = [];

  public async init() {
    log.info(`AniLinker
Copyright (C) 2019 selfisekai <today@selfisekai.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.`);
    this.connection = await createConnection();
    log.info('init done');
    log.debug('you are seeing debug logs');
  }

  public async start() {
    const SIMULTANEUOS_SERVICES_RUNNING_LIMIT = 3;
    const servicesPath = path.join(__dirname, 'services');
    const services = fs.readdirSync(servicesPath);
    // to work in both node and ts-node
    const [, currentFileExtension] = /\.([a-zA-Z0-9]+)$/.exec(__filename) || [, 'js'];
    services.forEach((service) => {
      const Service = require(path.join(servicesPath, service, `index.${currentFileExtension}`)).default;
      this.services.push(new Service());
    });

    const simultaneuosServices = Math.min(this.services.length, SIMULTANEUOS_SERVICES_RUNNING_LIMIT);
    for (let i = 0; i < simultaneuosServices; i += 1) {
      this.startReadyService();
    }
  }

  public async startService(name: string) {
    const service = this.services.find(s => s.service === name);
    if (!service) return;
    service.end(this.handleServiceClose(service));
    service.fetch()
      .catch(this.handleServiceClose(service));
  }

  public async startReadyService() {
    const service = this.services.find(s => s.state === ServiceState.READY);
    if (!service) return;
    service.end(this.handleServiceClose(service));
    service.fetch()
      .catch(this.handleServiceClose(service));
  }

  public handleServiceClose(service: BaseService & IBaseService) {
    return (r: any) => {
      if (r instanceof Error) {
        service.service = ServiceState.ERROR;
        log.error(service.service, r);
      }
      this.startReadyService();
    };
  }

  public async stop() {}
}
