import AniLinker from './AniLinker';

const AL = new AniLinker();

AL.init()
  .then(() => AL.start());
