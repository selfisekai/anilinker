import { got } from '../../request';
import log from 'consola';
import { BaseService, IBaseService, ServiceState, BaseMedia } from '../../BaseService';
import { MediaName } from '../../entities/media-name';
import { standarizeLanguage, sleep, standarizeExternalId } from '../../utils';
import { MediaType, MediaSubtype } from '../../entities/media';
import { MediaExternalId } from '../../entities/media-external-id';
import gql from 'graphql-tag';
import { print as graphqlPrint } from 'graphql';

export default class AniList extends BaseService implements IBaseService {
  service = 'anilist';

  constructor() {
    super();
  }

  public async fetch() {
    this.state = ServiceState.RUNNING;

    const query = gql`
      query Query($page: Int!) {
        Page(page: $page, perPage: 50) {
          pageInfo {
            hasNextPage
          }
          media {
            id
            idMal
            type
            format
            title {
              romaji
              english
              native
            }
            synonyms
            countryOfOrigin
            externalLinks {
              url
            }
            coverImage {
              large
            }
            isAdult
          }
        }
      }
    `;

    let i = 1;
    let continousFails = 0;
    const SLEEP_TIME_BETWEEN_REQUESTS = 60000 / 90; // 90 reqs per minute
    const SLEEP_TIME_ON_FAIL = 60000; // 1 minute
    const SLEEP_TIME_ON_CONTINOUS_FAIL = 30000;
    let isThereNextPage = true;
    for (;;) {
      const sleeping = sleep(SLEEP_TIME_BETWEEN_REQUESTS);
      await got.post('https://graphql.anilist.co/', {
        body: JSON.stringify({
          query: graphqlPrint(query),
          variables: {
            page: i,
          },
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(async (res) => {
          log.debug(`anilist p${i} loaded successfully`);
          const data = JSON.parse(res.body).data;
          Promise.all((data.Page.media as any[]).map(async (med) => {
            const type = med.type as MediaType; // fully compatible
            const titles = Object.keys(med.title)
              .filter(t => med.title[t])
              .map((t) => {
                const name = standarizeLanguage(t, med.countryOfOrigin);
                name.name = med.title[t];
                return name;
              })
              .concat((med.synonyms as string[]).map((s) => {
                const name = new MediaName();
                name.name = s;
                name.alternative = true;
                return name;
              }));
            const subtype = med.format as MediaSubtype; // https://anilist.github.io/ApiV2-GraphQL-Docs/mediaformat.doc.html
            const otherExternalIDs: MediaExternalId[] =
              (await Promise.all((med.externalLinks as { url: string }[])
                .map(({ url }) => standarizeExternalId({ url, type }))))
                .filter(e => !!e) as MediaExternalId[];
            if (med.idMal) {
              const malExternalId = await standarizeExternalId({
                type,
                id: med.idMal.toString(),
                service: type === MediaType.ANIME ? 'mal-anime' : 'mal-manga',
              });
              if (malExternalId) {
                otherExternalIDs.push(malExternalId);
              }
            }

            return {
              type,
              titles,
              subtype,
              otherExternalIDs,
              externalId: med.id.toString(),
              url: `https://anilist.co/${med.type.toLowerCase()}/${med.id}`,
              thumbnails: [med.coverImage.large],
              nsfw: med.isAdult,
            };
          })).then(queue => this.addMedia(queue));
          if (data.Page.pageInfo.hasNextPage) {
            i += 1;
            continousFails = 0;
            await sleeping;
          } else {
            isThereNextPage = false;
          }
        })
        .catch(async (err) => {
          log.error(`anilist p${i}`, err);
          await sleep(SLEEP_TIME_ON_FAIL
            + (continousFails * SLEEP_TIME_ON_CONTINOUS_FAIL));
          continousFails += 1;
        });
      if (!isThereNextPage) {
        this.emitEnd();
        break;
      }
    }
  }
}
