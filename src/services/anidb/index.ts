import sax from 'sax';
import { got } from '../../request';
import log from 'consola';
import { createUnzip } from 'zlib';
import { MediaName } from '../../entities/media-name';
import { standarizeLanguage } from '../../utils';
import { BaseService, IBaseService, BaseMedia, ServiceState } from '../../BaseService';
import { MediaType } from '../../entities/media';

export default class AniDB extends BaseService implements IBaseService {
  service = 'anidb';

  constructor() {
    super();
  }

  public async fetch() {
    this.state = ServiceState.RUNNING;
    const saxStream = sax.createStream(true, {
      trim: true,
      normalize: true,
    });

    const MAIN_TAG = 'animetitles';
    const ANIME_TAG = 'anime';
    const TITLE_TAG = 'title';

    let currentAnimeId: string | null = null;
    let currentTitleTag: sax.QualifiedTag | sax.Tag | null = null;
    let currentAnimeTitles: MediaName[] = [];

    saxStream.on('opentag', (tag) => {
      switch (tag.name) {
        case MAIN_TAG:
          log.debug('anidb file loaded');
          break;

        case ANIME_TAG:
          currentAnimeId = tag.attributes['aid'] as string;
          break;

        case TITLE_TAG:
          currentTitleTag = tag;
      }
    });

    saxStream.on('text', (text) => {
      if (currentAnimeId && currentTitleTag) {
        const xmllang = currentTitleTag.attributes['xml:lang'];
        const nameType = currentTitleTag.attributes['type'] as 'main' | 'official' | 'short' | 'syn';
        const animeName = typeof xmllang === 'string' ? standarizeLanguage(xmllang) : new MediaName();
        animeName.name = text;
        switch (nameType) {
          case 'main':
            animeName.main = true;
          case 'official':
            animeName.official = true;
            break;
          case 'short':
          case 'syn':
            animeName.alternative = true;
            break;
        }
        currentAnimeTitles.push(animeName);
      }
    });

    saxStream.on('closetag', (tag) => {
      switch (tag) {
        case ANIME_TAG:
          if (currentAnimeId && currentAnimeTitles.length > 0) {
            this.addMedia({
              type: MediaType.ANIME,
              titles: currentAnimeTitles,
              externalId: currentAnimeId,
              url: `https://anidb.net/a${currentAnimeId}`,
            } as BaseMedia);
          }
          currentAnimeTitles = [];
          currentAnimeId = null;
          break;
      }
    });

    got.stream.get('https://anidb.net/api/anime-titles.xml.gz', { decompress: false })
      .pipe(createUnzip())
      .pipe(saxStream)
      .end(() => this.emitEnd());
  }
}
