import { MediaName } from './entities/media-name';
import { MediaExternalId } from './entities/media-external-id';
import URL from 'url';
import { got } from './request';
import gql from 'graphql-tag';
import { print as graphqlPrint } from 'graphql';
import { MediaType } from './entities/media';

export const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export const countryToLang = (country: string) => {
  switch (country.toLowerCase()) {
    case 'jp':
      return 'ja';
    case 'kr':
      return 'kr';
    case 'cn':
      return 'zh';
  }
};

export const standarizeLanguage = (src: string, country?: string) => {
  const animeName = new MediaName();
  animeName.lang = null;
  animeName.romaji = false;
  if (src.length === 2) {
    animeName.lang = src;
  }
  switch (src.toLowerCase()) {
    case 'english':
    case 'en_gb':
    case 'en_us':
      animeName.lang = 'en';
      break;
    case 'native':
      if (country) {
        animeName.lang = countryToLang(country);
        break;
      }
    case 'japanese':
    case 'ja_jp':
      animeName.lang = 'ja';
      break;
    case 'alternative':
    case 'synonym':
    case 'synonyms':
      animeName.alternative = true;
      break;
    case 'x-jat':
    case 'en_jp':
      animeName.lang = 'ja';
    case 'romaji':
      if (country) {
        animeName.lang = countryToLang(country) || animeName.lang;
      }
      animeName.romaji = true;
      break;
  }
  // if still nothing
  if (!animeName.lang) {
    const [, lang] = /^([a-z]{2})-[A-Za-z]{2,}/.exec(src) || [, null];
    if (lang) {
      animeName.lang = lang;
    }
  }
  return animeName;
};

export const standarizeExternalId = async ({ url: lnk, id, service, type }:
  { url?: string, id?: string, service?: string, type: MediaType }) => {
  const ext = new MediaExternalId();
  if (lnk) {
    const url = URL.parse(lnk);
    if (!url.hostname || !url.pathname) return;
    const [, topDomain] = /[^.]+\.[^.]+$/.exec(url.hostname) || [, url.hostname];
    if (service) {
      ext.service = service;
    } else {
      switch (topDomain) {

        case 'myanimelist.net':
          const [, maltype] = /^\/(anime|manga)/.exec(url.pathname) || [, null];
          if (maltype === 'anime') {
            ext.service = 'mal-anime';
          } else if (maltype === 'manga') {
            ext.service = 'mal-manga';
          }
          break;

        case 'kitsu.io':
          const [, kitsutype] = /^\/(anime|manga)/.exec(url.pathname) || [, null];
          if (kitsutype === 'anime') {
            ext.service = 'kitsu-anime';
          } else if (kitsutype === 'manga') {
            ext.service = 'kitsu-manga';
          }
          break;

        case 'anilist.co':
          ext.service = 'anilist';
          break;

        case 'crunchyroll.com':
          ext.service = 'crunchyroll';
          break;

        case 'animelab.com':
          ext.service = 'animelab';
          break;

        case 'hulu.com':
          ext.service = 'hulu';
          break;

        case 'netflix.com':
          ext.service = 'netflix';
          break;

        case 'funimation.com':
          ext.service = 'funimation';
          break;

        case 'hidive.com':
          ext.service = 'hidive';
          break;

        case 'viz.com':
          ext.service = 'viz';
          break;

        case 'amazon.com':
          ext.service = 'amazon';
          break;

        case 'viewster.com':
          ext.service = 'viewster';
          break;

        case 'vrv.co':
          ext.service = 'vrv';
          break;

      }
    }
    if (id) {
      ext.externalId = id;
    } else {
      switch (topDomain) {

        case 'kitsu.io':
          // kitsu mostly uses slugs for the urls but a dev mostly needs the id to work
          // so if it's not provided, it must be fetched manually
          const [, type, slug] = /^\/(anime|manga)\/([a-z][0-9a-z-]*)/.exec(url.pathname) || [, null, null];
          if (type === 'anime' && slug) {
            const kitsuquery = gql`
              query Query($slug: [String!]!) {
                anime(slug: $slug) {
                  nodes {
                    id
                  }
                }
              }
            `;
            const r = await got.post('https://kitsu.io/api/graphql', {
              body: JSON.stringify({
                query: graphqlPrint(kitsuquery),
                variables: {
                  slug,
                },
              }),
            });
            ext.externalId = JSON.parse(r.body).data.anime.nodes[0].id;
            break;
          }
        case 'myanimelist.net':
        case 'anilist.co':
          const [, i] = /^\/(?:anime|manga)\/([0-9]+)/.exec(url.pathname) || [, null];
          ext.externalId = i;
          break;

        case 'crunchyroll.com':
        case 'animelab.com':
        case 'hulu.com':
        case 'netflix.com':
        case 'funimation.com':
        case 'hidive.com':
        case 'viz.com':
        case 'amazon.com':
        case 'viewster.com':
        case 'vrv.co':
          // don't care, no one will make apps anyway
          ext.externalId = null;
          break;

      }
    }
  } else if (id && service) {
    ext.externalId = id;
    ext.service = service;

    switch (service) {
      case 'mal-anime':
        ext.url = `https://myanimelist.net/anime/${id}/`;
        break;
      case 'mal-manga':
        ext.url = `https://myanimelist.net/manga/${id}/`;
        break;
      case 'anilist':
        ext.url = `https://anilist.co/${type.toLowerCase()}/${id}/`;
        break;
      case 'kitsu-anime':
        // they use slugs mostly but this works too (redirects user to the content)
        ext.url = `https://kitsu.io/anime/${id}`;
        break;
      case 'kitsu-manga':
        // they use slugs mostly but this works too (redirects user to the content)
        ext.url = `https://kitsu.io/manga/${id}`;
        break;
    }
  }
  if (!ext.url || !ext.service) {
    return;
  }
  return ext;
};
