import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { MediaName } from './media-name';
import { MediaExternalId } from './media-external-id';
import { MediaThumbnail } from './media-thumbnail';

export enum MediaType {
  ANIME = 'ANIME',
  MANGA = 'MANGA',
}
export enum AnimeSubtype {
  TV = 'TV',
  TV_SHORT = 'TV_SHORT',
  OVA = 'OVA',
  ONA = 'ONA',
  MOVIE = 'MOVIE',
  MUSIC = 'MUSIC',
  SPECIAL = 'SPECIAL',
}
export enum MangaSubtype {
  MANGA = 'MANGA',
  NOVEL = 'NOVEL',
  ONE_SHOT = 'ONE_SHOT',
  DOUJIN = 'DOUJIN',
}
export type MediaSubtype = AnimeSubtype | MangaSubtype;

@Entity()
export class Media {

  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('enum', {
    enum: ['ANIME', 'MANGA'],
  })
  type: MediaType;

  @Column('enum', {
    enum: [
      'TV',
      'TV_SHORT',
      'OVA',
      'ONA',
      'MOVIE',
      'SPECIAL',
      'MANGA',
      'NOVEL',
      'ONE_SHOT',
      'DOUJIN',
      'MUSIC',
    ],
    nullable: true,
  })
  subtype?: MediaSubtype;

  @Column('boolean', { nullable: true })
  nsfw?: boolean | null; // not guaranteed to be true

  @OneToMany(type => MediaName, mediaName => mediaName.media)
  names: MediaName[];

  @OneToMany(type => MediaExternalId, extId => extId.media)
  externalIds: MediaExternalId[];

  @OneToMany(type => MediaThumbnail, thumbnail => thumbnail.media)
  thumbnails: MediaThumbnail[];
}
