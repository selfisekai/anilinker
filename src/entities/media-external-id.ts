import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Media } from './media';

@Entity()
export class MediaExternalId {

  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(type => Media, media => media.externalIds)
  media: Media;

  @Column('varchar')
  service: string; // not enum because there will be just too many

  // nullable because for some services (mainly streaming) there's no id in url
  // and since they're anyway propertiary, closed and DRMed we just don't care
  // there always is the url
  @Column('varchar', { nullable: true })
  externalId?: string | null;

  @Column('varchar')
  url: string;
}
