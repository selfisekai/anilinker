import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Media } from './media';

@Entity()
export class MediaName {

  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(type => Media, media => media.names)
  media: Media;

  @Column('varchar')
  name: string;

  @Column('varchar', {
    length: 2,
    nullable: true,
  })
  lang?: string | null;

  @Column('boolean', { default: false })
  main: boolean;

  @Column('boolean', { default: false })
  romaji: boolean; // transcribed to roman alphabet

  @Column('boolean', { default: false })
  official: boolean;

  @Column('boolean', { default: false })
  alternative: boolean; // like shortened or synonyme
}
