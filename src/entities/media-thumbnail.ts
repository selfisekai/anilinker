import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Media } from './media';

@Entity()
export class MediaThumbnail {

  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('varchar')
  url: string;

  @Column('varchar')
  sourceService: string;

  @ManyToOne(type => Media, media => media.thumbnails)
  media: Media;
}
