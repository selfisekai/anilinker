import { getConnection, Repository } from 'typeorm';
import { Media, MediaType, MediaSubtype } from './entities/media';
import { MediaExternalId } from './entities/media-external-id';
import { MediaName } from './entities/media-name';
import { MediaThumbnail } from './entities/media-thumbnail';
import { isNotNullOrUndefined } from 'apollo-env';

export interface BaseMedia {
  type: MediaType;
  subtype?: MediaSubtype;
  titles: MediaName[];
  externalId: string;
  url: string;
  thumbnails?: string[];
  otherExternalIDs?: MediaExternalId[];
  nsfw?: boolean | null;
}

export enum ServiceState {
  READY = 'ready', // not started yet
  RUNNING = 'running', // during work
  ENDED = 'ended', // done its work successfully
  ERROR = 'error', // it was running but stopped because of error
}

export interface IBaseService {
  service: string;
  fetch: () => Promise<void>;
  state: ServiceState;
}

export class BaseService {
  mediaRepository: Repository<Media>;
  mediaExternalIdRepository: Repository<MediaExternalId>;
  mediaNameRepository: Repository<MediaName>;
  mediaThumbnailRepository: Repository<MediaThumbnail>;
  service: string;
  state: ServiceState;
  onEnd: ((service: string) => void)[];

  constructor() {
    const connection = getConnection();
    this.mediaRepository = connection.getRepository(Media);
    this.mediaExternalIdRepository = connection.getRepository(MediaExternalId);
    this.mediaNameRepository = connection.getRepository(MediaName);
    this.mediaThumbnailRepository = connection.getRepository(MediaThumbnail);
    this.state = ServiceState.READY;
    this.onEnd = [];
  }

  public end(f: (service: string) => void) {
    this.onEnd.push(f);
  }

  protected emitEnd() {
    this.state = ServiceState.ENDED;
    this.onEnd.forEach(f => f(this.service));
  }

  // many duplicate code and irrevelant value names, to refactor
  protected async addMedia(media__: BaseMedia | BaseMedia[]) {
    const media_ = Array.isArray(media__) ? media__ : [media__];
    media_.forEach(async (media) => {
      const namesToCheck = media.titles
        // no merging by native alphabet titles or abbreviations
        // https://gitlab.com/selfisekai/anilinker/issues/6
        .filter(m => !m.alternative
          && !m.romaji && (m.lang && !['ja', 'ko', 'zh'].includes(m.lang) || true))
        .map(m => m.name);
      const savedNames = await this.mediaNameRepository
        .createQueryBuilder('media_name')
        .select(['media_name.name', 'media.id', 'media.type', 'media.subtype', 'media.nsfw'])
        .leftJoinAndSelect('media_name.media', 'media')
        .where('media_name.name IN (:...namesToCheck)', { namesToCheck })
        .andWhere('media.type = :type', { type: media.type })
        .getMany();

      if (savedNames.length > 0
        && !savedNames.every(an => an.media.id === savedNames[0].media.id)) {

        return;
      }

      let theMedia = savedNames[0] ? savedNames[0].media : null;
      if (!theMedia) {
        theMedia = new Media();
        theMedia.type = media.type;
        theMedia.subtype = media.subtype;
        theMedia.nsfw = media.nsfw;
        await this.mediaRepository.save(theMedia);
      } else {
        let changed = false;
        if (!theMedia.subtype && media.subtype) {
          theMedia.subtype = media.subtype;
          changed = true;
        }
        if (!theMedia.nsfw && isNotNullOrUndefined(media.nsfw)) {
          theMedia.nsfw = media.nsfw;
          changed = true;
        }
        if (changed) {
          await this.mediaRepository.save(theMedia);
        }
      }

      let newNames: MediaName[] = [];
      let newThumbnails: MediaThumbnail[] = [];
      const newExternalId = new MediaExternalId();
      let otherExternalIds: MediaExternalId[] = [];

      newExternalId.externalId = media.externalId;
      newExternalId.url = media.url;
      newExternalId.service = this.service;
      newExternalId.media = theMedia;

      newNames = media.titles;
      newThumbnails = (media.thumbnails || []).map((t) => {
        const newThumbnail = new MediaThumbnail();
        newThumbnail.url = t;
        newThumbnail.sourceService = this.service;
        return newThumbnail;
      });
      otherExternalIds = media.otherExternalIDs || [];

      function attachMediaId<T>(ent: T): T {
        // @ts-ignore
        ent.media = theMedia;
        return ent;
      }
      otherExternalIds = otherExternalIds.map(attachMediaId);
      newNames = newNames.map(attachMediaId);
      newThumbnails = newThumbnails.map(attachMediaId);

      this.mediaExternalIdRepository.save([newExternalId].concat(otherExternalIds));
      this.mediaNameRepository.save(newNames);
      this.mediaThumbnailRepository.save(newThumbnails);
    });
  }
}
