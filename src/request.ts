import Got from 'got';
import { CookieJar } from 'tough-cookie';
import KeyvRedis from '@keyv/redis';
import { readJSONSync } from 'fs-extra';
import path from 'path';

export const cookieJar = new CookieJar();
export const cache = new KeyvRedis(process.env.REDIS_URL || 'redis://redis:pass@localhost:6379');

const { ADMIN_CONTACT } = process.env;
const { version } = readJSONSync(path.join(__dirname, '..', 'package.json'));

export const got = Got.extend({
  cookieJar,
  cache,
  headers: {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
      + ' Chrome/75.0.3770.102 Safari/537.36 Vivaldi/2.6.1566.44'
      + ` AniLinker/${version} (https://gitlab.com/selfisekai/anilinker; contact: ${ADMIN_CONTACT})`,
  },
});
