import { createConnection, Connection } from 'typeorm';
import log from 'consola';
import api from './api';
import Koa from 'koa';

class WebServer {
  connection: Connection;
  koa: Koa;

  public async init() {
    this.connection = await createConnection();
    this.koa = new Koa();
    api(this.koa);
  }

  public async start() {
    this.koa.listen(3000, () => log.start('Listening'));
  }
}

const web = new WebServer();
web.init()
  .then(() => web.start());
